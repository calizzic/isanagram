class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        #init dictionary
        letters = {}
        for c in s:
            if c in letters.keys():
                letters[c]+=1
            else:
                letters[c] = 1
        for c in t:
            if c in letters.keys():
                if letters[c]>0:
                    letters[c]-=1
                else:
                    return False
            else:
                return False
        for v in letters.values():
            if v !=0:
                return False
        return True